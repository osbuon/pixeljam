extends Area3D

const speed = 300
const duration = 5
@export var material: Material
var life = 0

func _ready():
	$MeshInstance.material_override = material

func body_entered(character):
	character.hp -= 1
	life = duration
	
	if character.hp <= 0:
		character.destroy()

func _physics_process(delta):
	position += transform.basis.z * speed * delta
	
	life += delta
	if life >= duration:
		queue_free()
