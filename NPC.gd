extends Node3D

@export var speed = 30
@export var rotation_speed = 0.5
@export var hp = 1

func _ready():
	add_to_group("Ally")

func move(delta, target):
	get_parent().velocity = global_transform.basis.z * speed
	
	var direction = target.position - global_position
	
	if global_transform.basis.x.dot(direction) > 0:
		get_parent().rotate(Vector3.UP, rotation_speed * delta)
	else:
		get_parent().rotate(Vector3.UP, - rotation_speed * delta)
	
	if global_transform.basis.y.dot(direction) > 0:
		get_parent().rotate(global_transform.basis.x, - rotation_speed * delta)
	else:
		get_parent().rotate(global_transform.basis.x, rotation_speed * delta)
	
	if global_transform.basis.x.dot(direction.normalized()) < 0.1 and global_transform.basis.y.dot(direction.normalized()) < 0.1:
		$Shooter.shoot()

	get_parent().move_and_slide()
