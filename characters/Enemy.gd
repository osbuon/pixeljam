extends CharacterBody3D

@export var hp = 1
var target: CharacterBody3D = null

func destroy():
	queue_free()
	
	var explosion = preload("res://Explosion.tscn").instantiate()
	get_tree().root.add_child(explosion)
	explosion.position = position

func _physics_process(delta):
	if target == null:
		var nodes = get_tree().get_nodes_in_group("Allies")
		if nodes.size() > 0:
			target = nodes[randi() % nodes.size()]
	
	if target != null:
		$NPC.move(delta, target)
