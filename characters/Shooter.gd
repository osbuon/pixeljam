extends Node3D

@export var material: Material
@export var delay = 0.5
var time = 0

func _physics_process(delta):
	time = max(0, time - delta)

func shoot():
	if time > 0:
		return
	
	var plasma = preload("res://Plasma.tscn").instantiate()
	plasma.material = material
	get_tree().root.add_child(plasma)
	plasma.position = global_position
	plasma.position += global_transform.basis.z * 20
	plasma.rotation = global_rotation
	
	time = delay
