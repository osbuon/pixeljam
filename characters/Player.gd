extends CharacterBody3D

@export var speed = 30
@export var rotation_speed = 1
@export var hp_max = 3
var hp = hp_max
var roll = 0

func destroy():
	var explosion = preload("res://Explosion.tscn").instantiate()
	get_tree().root.add_child(explosion)
	explosion.position = position
	
	position = Vector3(0, 0, 50)
	roll = 0
	hp = hp_max

func _physics_process(delta):
	velocity = transform.basis.z * speed
	if Input.is_action_pressed("boost"):
		velocity *= 2
	if Input.is_action_pressed("unboost"):
		velocity /= 2
	
	if Input.is_action_pressed("move_right"):
		rotate(Vector3.UP, - rotation_speed * delta)
		var r = rotation_speed * delta / 2
		roll += r
		rotate(transform.basis.z, r)
	if Input.is_action_pressed("move_left"):
		rotate(Vector3.UP, rotation_speed * delta)
		var r = - rotation_speed * delta / 2
		roll += r
		rotate(transform.basis.z, r)
	if Input.is_action_pressed("move_up"):
		rotate(transform.basis.x, rotation_speed * delta)
	if Input.is_action_pressed("move_down"):
		rotate(transform.basis.x, - rotation_speed * delta)
	
	if roll > 0.1:
		var r = - rotation_speed * delta / 4
		roll += r
		rotate(transform.basis.z, r)
	elif roll < -0.1:
		var r = rotation_speed * delta / 4
		roll += r
		rotate(transform.basis.z, r)
	
	if Input.is_action_pressed("shoot"):
		$Shooter.shoot()

	move_and_slide()
