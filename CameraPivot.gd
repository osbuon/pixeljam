extends Marker3D

func _process(delta):
	var player = $"../Characters/Player"
	var speed = player.speed
	var rotation_speed = player.rotation_speed
	var direction = (player.position + player.transform.basis.y * 20) - position
	
	var s = speed
	if direction.length() < 30:
		s /= 2
	position += transform.basis.z * s * delta
	
	if transform.basis.x.dot(direction) > 10:
		rotate(Vector3.UP, rotation_speed * delta)
	elif transform.basis.x.dot(direction) < -10:
		rotate(Vector3.UP, - rotation_speed * delta)
	
	if transform.basis.y.dot(direction) > 10:
		rotate(transform.basis.x, - rotation_speed * delta)
	elif transform.basis.y.dot(direction) < -10:
		rotate(transform.basis.x, rotation_speed * delta)
