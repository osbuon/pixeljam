extends Camera3D

@export var fov_multiplier = 500.

func _process(_delta):
	fov = atan(fov_multiplier / ($"../../Characters/Player".position - global_position).length()) * 180 / PI
	
	look_at($"../../Characters/Player".position, Vector3.UP)
