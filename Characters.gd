extends Node

@export var allies_count = 10
@export var enemies_count = 10
@export var spawn_dispersion = 1000

func random_pos():
	return -spawn_dispersion + randi() % (2*spawn_dispersion)

func _physics_process(_delta):
	var allies = get_tree().get_nodes_in_group("Allies")
	if allies.size() < allies_count:
		var character = preload("res://characters/Ally.tscn").instantiate()
		add_child(character)
		character.position = Vector3(random_pos(), random_pos(), random_pos())
	
	var enemies = get_tree().get_nodes_in_group("Enemies")
	if enemies.size() < enemies_count:
		var character = preload("res://characters/Enemy.tscn").instantiate()
		add_child(character)
		character.position = Vector3(random_pos(), random_pos(), random_pos())
